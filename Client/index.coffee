axios = require 'axios'
parser = require './parser'

# =====================================================================
# === Config ==========================================================
apiRoot     = 'https://4.bctotal.com/parsing/'  # Адрес сервера
interval    = 24 * 60 * 60 * 1000               # 1 день - инервал
# === / ===============================================================
# =====================================================================

getLinks = () ->
    # axios.get apiRoot
    ['https://s5.sir.sportradar.com/greenbet/en/1/season/54533/h2h/2817/33/match/15511524',
    'https://s5.sir.sportradar.com/greenbet/en/1/season/54571/h2h/60/44/match/14736543']

processLink = (link) ->
    parser.parse link, (result) ->
        axios.post apiRoot, result

doIteration = () ->
    links = await getLinks()
    processLink link for link in links

# =====================================================================
# === Запуск ==========================================================
doIteration()
setInterval doIteration, interval
