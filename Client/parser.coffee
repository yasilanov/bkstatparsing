phantomjs = require 'phantomjs-prebuilt'
webdriverio = require 'webdriverio'
cheerio = require 'cheerio'

program = null
cbStack = []
callCbs = ->
    if program != null
        for cb in cbStack
            cb()

waitPhantom = (cb) ->
    if program == null
        cbStack.push cb
    else
        cb()

wdOpts =
    desiredCapabilities: { browserName: 'phantomjs' }
    port: 4445 # + process.env.id * 1

console.log 'webdriver', wdOpts, '--ignore-ssl-errors=true --load-images=false --webdriver=' + wdOpts.port

phantomjs.run '--webdriver=4445' #'--ignore-ssl-errors=true --load-images=false --webdriver=' + wdOpts.port
.then (_program) ->
    program = _program
    callCbs()

processPageBody = (body) ->
    console.log 'GOT body'

    $ = cheerio.load body,
        decodeEntities: false

    playedSince = $('.col-sm-2 .graphics-text-primary-fill.graphics-text-primary-color .text-center').html()

    primaryWins = $('.padding-medium.text-center.graphics-primary .h1 strong').html()
    secondaryWins = $('.padding-medium.text-center.graphics-secondary .h1 strong').html()
    draws = $('.col-sm-2 .factor-scaling .col-xs-10.col-xs-offset-1 .size-xxl').html()

    placeFirst = $('.col-sm-5.graphics-text-regular-color .col-xs-8 .col-xs-5.text-left .factor-scaling.no-margin-left text.graphics-text-primary-fill.size-m').html()
    placeSecond = $('.col-sm-5.graphics-text-regular-color .col-xs-8 .col-xs-5.text-right .factor-scaling.no-margin-right text.graphics-text-secondary-fill.size-m').html()

    scoreFirst = $('.col-sm-5.graphics-text-regular-color .col-xs-8 .col-xs-5.text-left .text-uppercase .size-m').html()
    scoreSecond = $('.col-sm-5.graphics-text-regular-color .col-xs-8 .col-xs-5.text-right .text-uppercase .size-m').html()

    firstHighest = $('.col-sm-5.graphics-text-regular-color .col-xs-4.text-right .margin-top-large .text-bold').find('*').contents().filter(() -> @type is 'text').text()
    secondHighest = $('.col-sm-5.graphics-text-regular-color .col-xs-4.text-left .margin-top-large .text-bold').find('*').contents().filter(() -> @type is 'text').text()

    firstTotal = $('.col-sm-5.graphics-text-regular-color .col-xs-4.text-right .margin-top-large .margin-top:nth-child(2) strong').html()
    secondTotal = $('.col-sm-5.graphics-text-regular-color .col-xs-4.text-left .margin-top-large .margin-top:nth-child(2) strong').html()

    firstAverage = $('.col-sm-5.graphics-text-regular-color .col-xs-4.text-right .margin-top-large .margin-top:nth-child(3) strong').html()
    secondAverage = $('.col-sm-5.graphics-text-regular-color .col-xs-4.text-left .margin-top-large .margin-top:nth-child(3) strong').html()

    firstPerfomance = $('.col-sm-5.graphics-text-regular-color:nth-child(1) .desktop-width-80.margin-top-header .flex.flex-items-xs-center').find('*').contents().filter(() -> @type is 'text').text()
    secondPerfomance = $('.col-sm-5.graphics-text-regular-color:nth-child(3) .desktop-width-80.margin-top-header .flex.flex-items-xs-center').find('*').contents().filter(() -> @type is 'text').text()

    rows = $('.sub-container.h2h.common > div')
    console.log 'ROWS', rows.length

    # Last meetings
    meetingsRows = $('.row.component-header + .row .col-xs-12 .row .col-xs-12 table tbody tr')
    meetingsData = []

    console.log 'meetings', meetingsRows.length
    for mt in meetingsRows
        mt = cheerio.load mt,
            decodeEntities: false

        date = mt('.mobile-width-5.desktop-width-10').html()
        continue if !date?

        tournament = mt('.mobile-width-5.text-right span.hidden-xs').html()
        team1 = mt('td:nth-child(3) .row.flex-items-xs-middle .col-xs-4:nth-child(1) .col-xs.flex-xs-basis-auto.wrap.text-right .row .col-xs-12 .hidden-xs-down.wrap').html()
        team2 = mt('td:nth-child(3) .row.flex-items-xs-middle .col-xs-4:nth-child(3) .col-xs.flex-xs-basis-auto.wrap.text-left .row .col-xs-12 .hidden-xs-down.wrap').html()
        score = mt('td:nth-child(3) .row.flex-items-xs-middle .col-xs-4:nth-child(2)').find('*').contents().filter(() -> @type is 'text').text()

        meetingsData.push {
            date
            tournament
            team1
            team2
            score
        }

    meetingsData.length = 5 if meetingsData.length > 5

    # Over / Under
    ou = $('.sub-container.h2h.common > div:nth-child(' + (rows.length) + ') > .col-xs-12 > .row:nth-child(2)')
    # console.log ou.html()

    # UEFA
    uefa = $('.sub-container.h2h.common > div:nth-child(' + (rows.length - 2) + ')')
    firstRow = uefa.find('.table.table-condensed tbody tr:nth-child(1)')
    secondRow = uefa.find('.table.table-condensed tbody tr:nth-child(2)')

    firstPos = firstRow.find('.text-center.no-padding-left.no-padding-right').html()
    secondPos = secondRow.find('.text-center.no-padding-left.no-padding-right').html()

    firstTitle = firstRow.find('.col-xs.flex-xs-basis-auto.wrap.text-left .hidden-xs-down.wrap').html()
    secondTitle = secondRow.find('.col-xs.flex-xs-basis-auto.wrap.text-left .hidden-xs-down.wrap').html()

    firstPlayed = firstRow.find('td:nth-child(5)').html()
    secondPlayed = secondRow.find('td:nth-child(5)').html()

    firstWins = firstRow.find('td:nth-child(6)').html()
    secondWins = secondRow.find('td:nth-child(6)').html()

    firstDraws = firstRow.find('td:nth-child(7)').html()
    secondDraws = secondRow.find('td:nth-child(7)').html()

    firstLoss = firstRow.find('td:nth-child(8)').html()
    secondLoss = secondRow.find('td:nth-child(8)').html()

    firstGoalsMade = firstRow.find('td:nth-child(9)').html()
    secondGoalsMade = secondRow.find('td:nth-child(9)').html()

    firstGoalsMissed = firstRow.find('td:nth-child(10)').html()
    secondGoalsMissed = secondRow.find('td:nth-child(10)').html()

    firstDiff = firstRow.find('td:nth-child(11)').html()
    secondDiff = secondRow.find('td:nth-child(11)').html()

    firstPts = firstRow.find('td:nth-child(12)').html()
    secondPts = secondRow.find('td:nth-child(12)').html()

    tableData = {
        firstPos
        secondPos

        firstTitle
        secondTitle

        firstPlayed
        secondPlayed

        firstWins
        secondWins

        firstDraws
        secondDraws

        firstLoss
        secondLoss

        firstGoalsMade
        secondGoalsMade

        firstGoalsMissed
        secondGoalsMissed

        firstDiff
        secondDiff

        firstPts
        secondPts
    }
    # lastBlock = $('.sub-container.h2h.common div:nth-child(12)').html()
    # console.log lastBlock

    rows = $('.sub-container.h2h.common div')

    # for row in rows
    #     $row = cheerio.load row,
    #         decodeEntities: false
    #     console.log $row.html()

    result = {
        playedSince

        primaryWins
        secondaryWins
        draws

        placeFirst
        placeSecond

        scoreFirst
        scoreSecond

        firstHighest
        secondHighest

        firstTotal
        secondTotal

        firstAverage
        secondAverage

        firstPerfomance
        secondPerfomance

        tableData

        meetingsData
    }

    console.log result

module.exports =
    parse: (link) ->
        waitPhantom ->
            webdriverio.remote(wdOpts).init()
            .url link
            .then () ->
                console.log 'loaded', arguments
            .waitForExist '.sub-container.h2h.common > div:nth-child(11)', 10000
            .catch ->
                console.log 'catch nthchild', arguments
            .then ->
                console.log 'similar loaded'
            .waitForExist '.sub-container.h2h.common > div.row:last-child > .col-xs-12 > .row:nth-child(2) table.table.table-condensed > tbody > tr', 10000
            .catch ->
                console.log 'catch nthchild', arguments
            .then ->
                console.log 'similar loaded'
            # .waitForExist '.christmass', 5000
            # .catch ->
            #     console.log 'catch christmass', arguments
            # .then ->
            #     console.log 'christmass loaded'
            .getHTML('html')
            .catch -> console.log 'catch html', arguments
            .then (body) ->
                console.log 'html recieved'
                processPageBody body, () ->
                    console.log 'done'
                    remote.end()
