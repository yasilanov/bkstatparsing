const App = require('./App.js');
var another = require('./Data.js');
global.app = new App();
global.Allparsed = '';
global.OnlyPrematchParsed = '';
global.OnlyFootbalParsed = '';
global.PrematchFootbalParsed = '';
var http = require('http');
var port = 8081;

var s = http.createServer();
s.on('request', function(request, response) {
    var url = require('url');
    var url_parts = url.parse(request.url, true);
    var query = url_parts.query;
    response.setHeader("Content-Type", "application/json; charset=utf-8");
    response.writeHead(200);
    if(query.onlyFootball == 'true' && query.onlyPrematch == 'true') response.write(global.PrematchFootbalParsed);
    else if(query.onlyFootball == 'true' && query.onlyPrematch != 'true') response.write(global.OnlyFootbalParsed);
    else if(query.onlyFootball != 'true' && query.onlyPrematch == 'true') response.write(global.OnlyPrematchParsed);
    else response.write(global.Allparsed);
    response.end();
});

s.listen(port);

//another.f1("wss://line.gbpremium.com");
//another.f1("wss://stage.gbpremium.com");
//another.f1("wss://playback.gbpremium.com");
//another.f1("wss://socket.gbpremium.com");
another.f1("wss://stage.gbpremium.com");

module.exports = {f1: function(websocketServerLocation){start(websocketServerLocation);}}