class App{
    constructor(){
        this.firstRun = true;
        this.sports = new Map();
        this.events = new Map();
    }
}

module.exports = App;