class Event{
    constructor(id, sportId, homeName, awayName, link, status) {
        this.id = id;
        this.sportId = sportId;
        this.homeName = homeName;
        this.awayName = awayName;
        this.link = link;
        this.status = status;
    }
}

module.exports = {
    f1: function(websocketServerLocation){start(websocketServerLocation);}
};
module.exports = Event;